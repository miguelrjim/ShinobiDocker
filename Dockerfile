#   ===========================================================================
#   ---------------------------------------------------------------------------
#       ShinobiPro Recommended Docker image (phusion/baseimage)
#   ---------------------------------------------------------------------------
#   ===========================================================================
ARG ARG_PHUSION_TAG="0.11"
FROM phusion/baseimage:${ARG_PHUSION_TAG}

# Build arguments ...
# Image version
ARG ARG_IMAGE_VERSION="0.1.1"

# ShinobiPro branch, defaults to dev
ARG ARG_APP_BRANCH=dev

# Persist app-reladted build arguments
ENV APP_BRANCH=${ARG_APP_BRANCH}

# Shinobi's version information
ARG ARG_APP_VERSION 

# The channel or branch triggering the build.
ARG ARG_APP_CHANNEL

# The commit sha triggering the build.
ARG ARG_APP_COMMIT

# Update Shinobi on every container start?
#   manual:     Update Shinobi manually. New Docker images will always retrieve the latest version.
#   auto:       Update Shinobi on every container start.
ARG ARG_APP_UPDATE=manual

# Build data
ARG ARG_BUILD_DATE

# Additional Node JS packages for Shinobi plugins, addons, etc.
ARG ARG_ADD_NODEJS_PACKAGES="mqtt"

# Define Node.js version to use (Issue #20: Ubuntu based images fail build caused by sqlite3 `node-pre-gyp`)
ARG ARG_NODEJS_VERSION_FULL="12.14.1"

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${ARG_BUILD_DATE} \
    org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="MIT" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.version="Image v${ARG_IMAGE_VERSION}, Shinobi v${ARG_APP_VERSION}" \
    org.label-schema.description="Shinobi Pro - The Next Generation in Open-Source Video Management Software" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-ref=${ARG_APP_COMMIT} \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/ShinobiDocker.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Persist app-reladted build arguments
ENV APP_VERSION=$ARG_APP_VERSION \
    APP_CHANNEL=$ARG_APP_CHANNEL \
    APP_COMMIT=$ARG_APP_COMMIT \
    APP_UPDATE=$ARG_APP_UPDATE \
    APP_BRANCH=${ARG_APP_BRANCH} \
    APP_FLAVOR=${ARG_FLAVOR} \
    APP_IMAGE_VERSION=${ARG_IMAGE_VERSION} \
    PUID=1000 \
    PGID=1000 \
    SERVER_NAME=shinobi.localdomain

WORKDIR /tmp/workdir

RUN \
    #   Install Nodejs
    curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt update \
    && apt install -y \
    nodejs \
    # Install package dependencies
    libfreetype6-dev \ 
    libgnutls28-dev \ 
    libmp3lame-dev \ 
    libass-dev \ 
    libogg-dev \ 
    libtheora-dev \ 
    libvorbis-dev \ 
    libvpx-dev \ 
    libwebp-dev \ 
    libssh2-1-dev \ 
    libopus-dev \ 
    librtmp-dev \ 
    libx264-dev \ 
    libx265-dev \ 
    yasm \
    build-essential \ 
    bzip2 \ 
    coreutils \ 
    gnutls-bin \ 
    nasm \ 
    tar \ 
    x264 \
    # Install additional 
    ffmpeg \
    git \
    libsqlite3-dev \
    make \
    mariadb-client \
    pkg-config \
    python \
    socat \
    sqlite \
    wget \
    tzdata \
    xz-utils \
    gettext-base \
    nginx \
    #   Nodejs addons
    && npm install -g npm@latest pm2

# Issue #20: Ubuntu based images fail build caused by sqlite3 `node-pre-gyp`
RUN npm install -g n \
    && n ${ARG_NODEJS_VERSION_FULL}

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copy file system sources
COPY sources/etc/ /etc/


RUN chmod +x /etc/my_init.d/*.sh \
    && find /etc/service -maxdepth 2 -name run -exec chmod +x "{}" \;

RUN groupadd -g ${PGID} appuser

RUN useradd -r -m -u ${PUID} -g ${PGID} appuser

RUN mkdir -p /opt/shinobi

RUN chown appuser:appuser /opt/shinobi

USER appuser

# Create additional directories for: Custom configuration, working directory, database directory, scripts

# Assign working directory
WORKDIR /opt/shinobi

# Install Shinobi app including NodeJS dependencies
RUN git clone -b ${APP_BRANCH} https://gitlab.com/Shinobi-Systems/Shinobi.git /opt/shinobi \
    && npm install sqlite3 --unsafe-perm \
    && npm install jsonfile edit-json-file ${ARG_ADD_NODEJS_PACKAGES} \
    # && npm install ffbinaries \ 
    && npm install --unsafe-perm \
    && npm audit fix --force

COPY --chown=${PUID}:${PGID} sources/opt/shinobi/ ./

USER root

COPY sources/nginx/default.template /tmp

RUN envsubst '$SERVER_NAME' < /tmp/default.template > /etc/nginx/sites-available/default

VOLUME [ "/opt/shinobi/conf.json", "/opt/shinobi/super.json", "/opt/shinobi/videos", "/ssl" ]

EXPOSE 80 443

CMD ["/sbin/my_init"]
